'''
Group 0: Avoiders of a list of mesh patterns

Example 1: Avoiders of (123,{(0,0),(1,1),(2,2),(3,3)}) and (123,{(0,3),(1,2),(2,1),(3,0)})
Example 2: Avoiders of (12,{(0,0),(1,1),(2,2)}) and (12,{(0,2),(1,1),(2,0)})

Group 1: Classes of Schubert varieties
TODO: Add dbi and lci varieties to this group

Example 1: Smooth Schubert varieties
Example 2: Factorial Schubert varieties
Example 3: Gorenstein Schubert varieties
Example 4: All corners lie on the same anti-diagonal (sub prop. of Gorenstein Schubert varieties)

Group 2: Various stuff

Example 1: Simple permutations
Example 2: Clumped permutations (maximum number of clumps <= 3 - can be changed)
Example 3: Simsun permutations
Example 4: Balanced permutations
Example 5: Menage permutations
Example 6: Kendall-Mann permutations
Example 7: Number of recoils <= 1
Example 8: Number of saliances (right-to-left minima) <= 1
Example 9: swapping number <= 1
Example 10: swapping number <= 2
Example 11: number of inversions = number of descents

THINGS TO ADD TO THIS Group
-Map a permutation to a word and use some properties from there
- Properties of the Rothe diagram

Group 3: Properties requiring manual construction

Example 1: Stamp folding
Example 2: Meanders
Example 3: Dumont permutations of the second kind

Group 4: Properties related to sorting

Example 1: 1-stack-sortables
Example 2: West-2-stack-sortables
Example 3: West-3-stack-sortables
Example 4: B^{-1}(Av(1243))
Example 5: Bubble-sorted permutations
Example 6: Stack-sorted permutations
Example 7: 1-quick-sortables (Henning's definition)
Example 8: 2-quick-sortables (Henning's definition)
Example 9: 1-quick-sortables (Anders's definition)
Example 10: 2-quick-sortables (Anders's definition)
Example 11: A combination of stack-sorting and applying inverse

Group 5: Properties of the set of fixed points

Example 1: == 0
Example 2: <= 1
Example 3: <= 2
Example 4: <= 3
For the next two see "The match set of a random permutation has the FKG property by Fishburn et al."
Example 5: There is an odd fixed point
Example 6: There is an odd and an even fixed point

Group 6: Multiplying mesh patterns

Example 1: (12,{(0,0),(1,1),(2,2)}) and (21,{(0,2),(1,1),(2,0)})
Example 2: (123,{}) and (132,{})

Group 7: Tableaux properties

TODO: Write a function that can accept the shape
TODO: Then add avoidance of (2,1), (2,1,1), (3,1)

THINGS TO ADD TO THIS Group
# Charge <= 1, C = 8, M = 5 gives three classical patterns of length 3
# Charge <= 2, C = 8, M = 5 gives six classical patterns of lengths 3 and 4
# Nr. of descents <= 1, C =9, M = 5 gives five nice classical patterns of lengths 3 and 4
# Nr. of descents <= 2, C =9, M = 5 gives a lot of classical patterns of lengths 4 and 5

Example 1: No (2,2)
Example 2: No (3,2)
Example 3: Number of corners <= 1

Group 8: Group-theoretic properties

Example 1: Involutions (order <= 2)
Example 2: No 2 in the cycle type
Example 3: Single cycles

Group 9: Properties of planar maps

Example 1: Primitive planar maps (avoiding three patterns)
'''

# Contains the main functions for dictionaries, also
# for Group 1: Avoiders of a list of mesh patterns
load '~/repos/basic_pattern_functions/basic_functions.sage'
load '~/repos/basic_pattern_functions/sorting_functions.sage'
load '~/repos/basic_pattern_functions/properties_of_permutations.sage'
# For Group 3, Example 1: Stamp folding permutations
load '~/repos/grim/known_dictionaries/Stamp_folding_perms.sage'
# For Group 3, Example 3: Dumont permutations of the second kind
load '~/repos/grim/known_dictionaries/Dumont_second_kind.sage'
# For Group 8, Example 1: Involutions (order <= 2)
from sage.combinat.permutation import robinson_schensted_inverse

def examples_for_GRIM(gr_num,ex_num,largest_good_perms,largest_bad_perms,w_compl):

	if gr_num == 0:
		print "Avoiders of a list of mesh patterns"

		if ex_num == 1:
			print "Avoiders of (123,{(0,0),(1,1),(2,2),(3,3)}) and (123,{(0,3),(1,2),(2,1),(3,0)})"
			mp1 = ([1,2,3],[(0,0),(1,1),(2,2),(3,3)])
			mp2 = ([1,2,3],[(0,3),(1,2),(2,1),(3,0)])

			prop = lambda x : avoids_mpats(x,[mp1,mp2])

		if ex_num == 2:
			print "Avoiders of (12,{(0,0),(1,1),(2,2)}) and (12,{(0,2),(1,1),(2,0)})"
			mp1 = ([1,2],[(0,0),(1,1),(2,2)])
			mp2 = ([1,2],[(0,2),(1,1),(2,0)])

			prop = lambda x : avoids_mpats(x,[mp1,mp2])

			prop = lambda x : avoids_mpats(x,[mp1,mp2])
	
	if gr_num == 1:
		print "Classes of Schubert varieties"

		if ex_num == 1:
			print "Smooth Schubert varieties"

			prop = lambda x : x.avoids([1,3,2,4]) and x.avoids([2,1,4,3])

		if ex_num == 2:
			print "Factorial Schubert varieties"

			prop = lambda x : x.avoids([1,3,2,4]) and avoids_mpat(x,[2,1,4,3],[(2,2)])

		if ex_num == 3:
			print "Gorenstein Schubert varieties"

			prop = lambda x : is_Gorenstein(x)

		if ex_num == 4:
			print "All corners lie on the same anti-diagonal (sub prop. of Gorenstein Schubert varieties)"

			prop = lambda x : all_corners_on_anti_diagonal(x)

	if gr_num == 2:
		print "Various examples"

		if ex_num == 1:
			print "Simple permutations"

			prop = lambda x : is_simple(x)

		if ex_num == 2:
			print "Clumped permutations (maximum number of clumps <= 3 - can be changed)"

			prop = lambda x : max_number_of_clumps(x) <= 3

		if ex_num == 3:
			print "Simsun permutations"

			prop = lambda x : avoids_mpat(x,[3,2,1],[(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)])

		if ex_num == 4:
			print "Balanced permutations"

			prop = lambda x : is_balanced(x)

		if ex_num == 5:
			print "Menage permutations"

			prop = lambda x : is_menage(x)

		if ex_num == 6:
			print "Kendall-Mann permutations"

			prop = lambda x : is_Kendall_Mann(x)

		if ex_num == 7:
			print "Number of recoils <= 1"

			prop = lambda x : x.number_of_recoils() <= 1

		if ex_num == 8:
			print "Number of saliances (right-to-left minima) <= 1"

			prop = lambda x : x.number_of_saliances() <= 1

		if ex_num == 9:
			print "swapping number <= 1"

			prop = lambda x : swapping_number(x) <= 1

		if ex_num == 10:
			print "swapping number <= 2"

			prop = lambda x : swapping_number(x) <= 2

		if ex_num == 11:
			print "number of inversions = number of descents"

			prop = lambda x : x.number_of_descents() == x.number_of_inversions()

	if gr_num == 3:
		print "Properties requiring manual construction"

		if ex_num == 1:
			print "Stamp folding permutations"

			D = dict()

			for k in [1 .. largest_good_perms]:

				D[k] = Stamp_folding_perms[k]

		if ex_num == 2:
			print "Meanders"

			Meanders= load('/Users/ulfarsson/repos/grim/known_dictionaries/' + 'Meanders' + '.sobj')

			D = dict()

			for k in [1 .. largest_good_perms]:

				D[k] = Meanders[k]

		if ex_num == 3:
			print "Dumont permutations of the second kind"

			D = dict()

			for k in [1 .. largest_good_perms]:

				D[k] = Dumont_second_kind[k]

		return func_calc_compl(D,w_compl)

	if gr_num == 4:
		print "Properties related to sorting"

		if ex_num == 1:
			print "1-stack-sortables"

			prop = lambda x : stack_sort(x) == Permutation([1..len(x)])

		if ex_num == 2:
			print "West-2-stack-sortables"

			prop = lambda x : stack_sort(stack_sort(x)) == Permutation([1..len(x)])

		if ex_num == 3:
			print "West-3-stack-sortables"

			prop = lambda x : stack_sort(stack_sort(stack_sort(x))) == Permutation([1..len(x)])

		if ex_num == 4:
			print "B^{-1}(Av(1243))"

			prop = lambda x : Permutation(bubble_sort(x)).avoids([1,2,4,3])

		if ex_num == 5:
			print "Bubble-sorted permutations"

			D = dict()

			for k in [1 .. largest_good_perms]:

				D[k] = []

				for w in Permutations(k):
					sperm = bubble_sort(w)
					if sperm not in D[k]:
						D[k].append(sperm)

			return func_calc_compl(D,w_compl)

		if ex_num == 6:
			print "Stack-sorted permutations"

			D = dict()

			for k in [1 .. largest_good_perms]:

				D[k] = []

				for w in Permutations(k):
					sperm = stack_sort(w)
					if sperm not in D[k]:
						D[k].append(sperm)

			return func_calc_compl(D,w_compl)

		if ex_num == 7:
			print "1-quick-sortables (Henning's definition)"

			prop = lambda x : is_1_quick_sortable(x)

		if ex_num == 8:
			print "2-quick-sortables (Henning's definition)"

			prop = lambda x : is_2_quick_sortable(x)

		if ex_num == 9:
			print "1-quick-sortables (Anders's definition)"

			prop = lambda x : Permutation(K(x)) == Permutation(range(1,len(x)+1))

		if ex_num == 10:
			print "2-quick-sortables (Anders's definition)"

			prop = lambda x : Permutation(K(K(x))) == Permutation(range(1,len(x)+1))

		if ex_num == 11:
			print "A combination of stack-sorting and applying inverse"

			prop = lambda x : stack_sort(Permutation(stack_sort(x)).inverse()) == Permutation([1..len(x)])

	if gr_num == 5:
		print "Properties of the set of fixed points"

		if ex_num == 1:
			print "== 0"

			prop = lambda x : x.number_of_fixed_points() == 0

		if ex_num == 2:
			print "<= 1"

			prop = lambda x : x.number_of_fixed_points() <= 1

		if ex_num == 3:
			print "<= 2"

			prop = lambda x : x.number_of_fixed_points() <= 2

		if ex_num == 4:
			print "<= 3"

			prop = lambda x : x.number_of_fixed_points() <= 3

		if ex_num == 5:
			print "There is an odd fixed point"

			prop = lambda x : any( map( lambda y : is_odd(y), x.fixed_points() ) )

		if ex_num == 6:
			print "There is an odd and an even fixed point"

			prop = lambda x : any( map( lambda y : is_odd(y) or is_even(y), x.fixed_points() ) )

	if gr_num == 6:
		print "Multiplying mesh patterns"

		if ex_num == 1:
			print "(12,{(0,0),(1,1),(2,2)}) and (21,{(0,2),(1,1),(2,0)})"

			prop1 = lambda x : avoids_mpat(x,[1,2],[(0,0),(1,1),(2,2)])
			prop2 = lambda x : avoids_mpat(x,[2,1],[(0,2),(1,1),(2,0)])

		if ex_num == 2:
			print "(123,{}) and (132,{})"

			prop1 = lambda x : avoids_mpat(x,[1,2,3],[])
			prop2 = lambda x : avoids_mpat(x,[1,3,2],[])

		A1 = para_perms_sat_prop(largest_good_perms,prop1)
		A2 = para_perms_sat_prop(largest_good_perms,prop2)

		D = multiply_dicts(largest_good_perms,A1,A2)

		return func_calc_compl(D,w_compl)

	if gr_num == 7:
		print "Tableaux properties"

		if ex_num == 1:
			print "No (2,2)"

			prop = lambda x : not any(map(lambda x: x > 1, x.left_tableau().shape()[1:] ))

		if ex_num == 2:
			print "No (3,2)"

			def check_func32(x):
				L = x.left_tableau().shape()
	        
				if L[0] < 3:
					return True
				else:
					if len(L) == 1:
						return True
					else:
						if L[1] < 2:
							return True
				return False

			prop = lambda x : check_func32(x)

		if ex_num == 3:
			print "Number of corners <= 1"

			prop = lambda x : len(x.left_tableau().corners()) <= 1

	if gr_num == 8:
		print "Group-theoretic properties"

		if ex_num == 1:
			print "Involutions (order <= 2)"

			D = dict()

			for k in [1 .. largest_good_perms]:

				D[k] = []

				for T in StandardTableaux(k):
					D[k].append(robinson_schensted_inverse(T,T))

			return func_calc_compl(D,w_compl)

		if ex_num == 2:
			print "No 2 in the cycle type"

			prop = lambda x : not 2 in x.cycle_type()

		if ex_num == 3:
			print "Single cycles"

			prop = lambda x : len(x.cycle_type()) == 1

	if gr_num == 9:
		print "Properties of planar maps"

		if ex_num == 1:
			print "Primitive planar maps (avoiding three patterns)"

			mp1 = [[3,1,4,2],[]]
			mp2 = [[2,4,1,3],[(2,0),(2,1),(2,2),(2,3),(2,4)]]
			mp3 = [[2,1],[(1,0),(1,1),(1,2),(2,1)]]

			prop = lambda x : avoids_mpats(x,[mp1,mp2,mp3])

	if w_compl:
		return para_perms_sat_prop_w_complement_different_sizes(largest_good_perms,largest_bad_perms,prop)
	else:
		return para_perms_sat_prop(largest_good_perms,prop)

def func_calc_compl(D,w_compl):
	if w_compl:
		prop = lambda x : x not in D[len(x)]
		return D, para_perms_sat_prop(largest_bad_perms,prop)
	else:
		return D