load '~/repos/basic_pattern_functions/basic_functions.sage'

# Helper function for find_badpatts and para_find_badpatts
def rec(C,forb,lst):
    
    #pruning lst
    newlst = []
    for L in lst:
        if L.issubset(forb):
            return []
        if not C.intersection(L):
            newlst.append(L)
    
    if newlst:
        lst0 = newlst[0]
        
        i = 0
        while lst0[i] in forb:
            i = i+1
            
        return rec( C.union(Set([lst0[i]])), forb, newlst ) + rec( C, forb.union(Set([lst0[i]])), newlst )
    else:
        return [C]

def rec_w_reduce(C,forb,lst,perm):
    
    global badpatts
    global check_interval

    #pruning lst
    newlst = []
    for L in lst:
        if L.issubset(forb): # If a member of lst is inside forb then it is impossible to satisfy that member
            return []
        if not C.intersection(L): # Here we find the members of lst that have not yet been satisfied
            newlst.append(L)
    
    if newlst:
        lst0 = newlst[0] # Now we aim for satisfying the first member of lst, lst0
        
        i = 0 # Here we find the first box in lst0 that we are allowed to fill in (i.e., put in C)
        while lst0[i] in forb:
            i = i+1

        '''
        This was added in September 2012.
        We now check if the addition of the box lst0[i] makes this entire branch in the
        recursion redundant.
        '''
        D = C.union(Set([lst0[i]]))
        for j in check_interval:
            if j == len(perm):
                break
            for cl_patt in badpatts[j]:
                if mesh_has_mesh_many_shadings((perm,D),cl_patt,badpatts[j][cl_patt]):
                    return rec_w_reduce( C, forb.union(Set([lst0[i]])), newlst, perm )
            
        return rec_w_reduce( D, forb, newlst, perm ) + rec_w_reduce( C, forb.union(Set([lst0[i]])), newlst, perm )
    else:
        return [C]

def rec_w_reduce_pattern_pos(C,forb,lst,perm,pattern_positions):
    
    global badpatts
    global check_interval

    #pruning lst
    newlst = []
    for L in lst:
        if L.issubset(forb): # If a member of lst is inside forb then it is impossible to satisfy that member
            return []
        if not C.intersection(L): # Here we find the members of lst that have not yet been satisfied
            newlst.append(L)
    
    if newlst:
        lst0 = newlst[0] # Now we aim for satisfying the first member of lst, lst0
        
        i = 0 # Here we find the first box in lst0 that we are allowed to fill in (i.e., put in C)
        while lst0[i] in forb:
            i = i+1

        '''
        This was added in September 2012.
        We now check if the addition of the box lst0[i] makes this entire branch in the
        recursion redundant.
        '''
        D = C.union(Set([lst0[i]]))
        for j in check_interval:
            if j == len(perm):
                break
            for cl_patt in badpatts[j]:
                if mesh_has_mesh_with_positions(perm, D, pattern_positions[cl_patt], badpatts[j][cl_patt]):
                    return rec_w_reduce_pattern_pos( C, forb.union(Set([lst0[i]])), newlst, perm, pattern_positions )
            
        return rec_w_reduce_pattern_pos( D, forb, newlst, perm, pattern_positions ) + rec_w_reduce_pattern_pos( C, forb.union(Set([lst0[i]])), newlst, perm, pattern_positions )
    else:
        return [C]

#
# This function checks if mesh_patt occurs in mesh_perm
#

def mesh_has_mesh_with_positions(perm, S, pattern_pos, Rs):

    # If there are no occurrences we return False
    if not pattern_pos:
        return False
    # Otherwise the length of the pattern we are looking at is given
    # by the length of the first occurrence of it in perm
    else:
        k = len(pattern_pos[0])

    n = len(perm)
    
    Gperm = G(perm)
    
    Scomp = Set(map(lambda x: tuple(x),CartesianProduct([0..n],[0..n]))).difference(S)

    for H in pattern_pos:
        X = dict( (x+1,y+1) for (x,y) in enumerate(H) )
        Y = dict( G(sorted(perm[j] for j in H)) )

        X[0], X[k+1] = 0, n+1
        Y[0], Y[k+1] = 0, n+1
        
        for R in Rs:
            shady = ( X[i] < x < X[i+1] and Y[j] < y < Y[j+1]\
                      for (i,j) in R\
                      for (x,y) in Gperm\
                    )
                    
            shaky = ( X[i] <= x < X[i+1] and Y[j] <= y < Y[j+1]\
                      for (i,j) in R\
                      for (x,y) in Scomp\
                    )
                    
            if not any(shady):
                if not any(shaky):
                      return True
    return False

def mesh_has_mesh(mesh_perm,mesh_patt):
    
    pat  = mesh_patt[0]
    R    = mesh_patt[1]
    
    perm = mesh_perm[0]
    S    = mesh_perm[1]
    
    k = len(pat)
    n = len(perm)

    if k > n:
        return False
    
    pat  = G(pat)
    perm = G(perm)
    
    Scomp = Set(map(lambda x: tuple(x),CartesianProduct([0..n],[0..n]))).difference(S)
    
    for H in Subwords(perm, k):
        
        X = dict(G(sorted(i for (i,_) in H)))
        Y = dict(G(sorted(j for (_,j) in H)))
        
        if H == [ (X[i], Y[j]) for (i,j) in pat ]:
            
            X[0], X[k+1] = 0, n+1
            Y[0], Y[k+1] = 0, n+1
            
            shady = ( X[i] < x < X[i+1] and Y[j] < y < Y[j+1]\
                      for (i,j) in R\
                      for (x,y) in perm\
                    )
                    
            shaky = ( X[i] <= x < X[i+1] and Y[j] <= y < Y[j+1]\
                      for (i,j) in R\
                      for (x,y) in Scomp\
                    )
                    
            if not any(shady):
                if not any(shaky):
                    return True
    return False

'''
Specialized version of the function above
'''
def mesh_has_mesh_many_shadings(mesh_perm,patt,Rs):
    
    perm = mesh_perm[0]
    S    = mesh_perm[1]
    
    k = len(patt)
    n = len(perm)

    if k > n:
        return False
    
    patt  = G(patt)
    perm = G(perm)
    
    Scomp = Set(map(lambda x: tuple(x),CartesianProduct([0..n],[0..n]))).difference(S)
    
    for H in Subwords(perm, k):
        
        X = dict(G(sorted(i for (i,_) in H)))
        Y = dict(G(sorted(j for (_,j) in H)))
        
        if H == [ (X[i], Y[j]) for (i,j) in patt ]:
            
            X[0], X[k+1] = 0, n+1
            Y[0], Y[k+1] = 0, n+1

            for R in Rs:
            
                shady = ( X[i] < x < X[i+1] and Y[j] < y < Y[j+1]\
                          for (i,j) in R\
                          for (x,y) in perm\
                        )
                        
                shaky = ( X[i] <= x < X[i+1] and Y[j] <= y < Y[j+1]\
                          for (i,j) in R\
                          for (x,y) in Scomp\
                        )
                        
                if not any(shady):
                    if not any(shaky):
                        return True
    return False

def describe_GRIM_output(OP):
    
    mult = 1
    flip = True
    
    for k in OP.keys():
        clpatts_of_this_length = OP[k].keys()
        print "There are " + str(len(clpatts_of_this_length)) + " underlying classical patterns of length " + str(k)
        for clpatt in clpatts_of_this_length:
            
            new_factor = len(OP[k][clpatt])
            mult = mult*new_factor
            
            print "There are " + str(new_factor) + " different shadings on " + str(clpatt)
        
        if flip:  
            print "The number of sets to monitor at the start of the clean-up phase is " + str(mult)
        flip = False

# Function that prints the sizes of all the sub-dictionaries of a big dictionary
def size_of_subdicts(D):
    for k in D.keys():
        print 'Perms of length ' + str(k) + ' with this property are ' + str(len(D[k]))

# Function checks whether the patterns in SG suffice to describe the permutations in the dictionary D up to length L
#
# THE STEP WHERE WE ARE COUNTING THE AVOIDERS SHOULD BE MADE FASTER BY CALLING THE FUNCTION ABOVE
#
def patterns_suffice(SG,L,D,compare=False):

    # We start by flattening SG into the old format
    # This should NOT be done.
    # We should make a special avoiders function that
    # uses the structure of SG to run faster
    
    for n in [1..L]:
    
        if n not in D.keys():
            print 'The dictionary does not contain permutations of length ' + str(n)
            break
        '''
        c = 0
        for w in Permutations(n):
    
            if avoids_mpats(w,SG) == True:
                c = c+1
        '''
        Cn = para_perms_in_avoiding_many_one_layer_many_shadings(n,SG)
        Dn = D[n]
        if compare:
            check = ( len(Cn) == len(Dn) and all(c in Dn for c in Cn))
        else:
            check = len(Cn) == len(Dn)
    
        if check:
            print 'The patterns correctly describe the good perms of length ' + str(n)
        else:
            print ''
            print 'The patterns fail to describe the good perms of length ' + str(n)
            print 'There are ' + str(len(Dn)) + ' good perms of length ' + str(n)
            print 'but there are ' + str(len(Cn)) + ' perms that avoid the patterns'
            break