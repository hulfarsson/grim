def clean_up(SG,perm_len_min,perm_len_max,patt_len_min,patt_len_max,report=False,detailed_report=False,limit_monitors=0):
    
    # Dictionaries to go between mesh patterns
    # and numbers (length, pattern number, shading number)
    dict_clpatts_to_numbs = dict()
    dict_numbs_to_patts = dict()
    
    SG_keys = SG.keys()
    
    # A dictionary that "looks" like SG, but has only numbers
    sg = dict()
    
    # Putting values in the three dictionaries
    for k in SG_keys:
        
        sg[k] = dict()
        
        patt_i = 0
        
        for clpatt in SG[k]:
            
            sh_i = 0
            for sh in SG[k][clpatt]:
                dict_clpatts_to_numbs[clpatt] = patt_i
                dict_numbs_to_patts[(k,patt_i,sh_i)] = (clpatt,sh)
                sh_i = sh_i+1
                
            sg[k][patt_i] = [0..sh_i-1]
                
            patt_i = patt_i+1
            
    # initialize len_calc_patts as the length of the smallest mesh patterns
    len_calc_patts = [patt_len_min .. patt_len_max] #[SG_keys[0]]
    
    # initialize monitor by taking one mesh pattern for
    # every bad permutation on the first level
    if report:
        print "Creating the sets to monitor"
        print ""
    monitor = one_from_each( [ [(len_calc_patts[0],x,y) for y in sg[len_calc_patts[0]][x]] for x in sg[len_calc_patts[0]].keys() ] )
    
    if report:
        print "Starting the tests"
    # perm_len_min should be 1+len_calc_patts[0]
    for L in [perm_len_min..perm_len_max]:
        if report:
                print "----------------------------------------------------------------"
                print "Testing permutations of length " + str(L)
                print ""

        for perm in B[L]:
            
            if report:
                print "-----------------------------------"
                print "Testing the permutation " + str(perm)
            
            if not monitor:
                print "No sets to monitor, try allowing longer patterns"
                return []
            
            loop_monitor = list(monitor)
            
            dict_numbs_to_perm_avoids_patt = dict()
            saviors = []
            
            for ell in filter( lambda x : x < L, len_calc_patts) :
                for patt_i in sg[ell].keys():
                    for sh_i in sg[ell][patt_i]:
                        
                        mpat = dict_numbs_to_patts[(ell,patt_i,sh_i)]
                        
                        if avoids_mpat(perm,mpat[0],mpat[1]):
                            dict_numbs_to_perm_avoids_patt[(ell,patt_i,sh_i)] = True
                        else:
                            dict_numbs_to_perm_avoids_patt[(ell,patt_i,sh_i)] = False
                            saviors.append((ell,patt_i,sh_i))
            h = -1
            capture_failure = False          
            for mon in loop_monitor:
                h = h+1
                if all(dict_numbs_to_perm_avoids_patt[m] for m in filter( lambda x : x[0]<L , mon )):
                    capture_failure = True
                    if detailed_report:
                        print "Monitor nr. " + str(h) + " failed"
                        print "This monitor consists of:"
                        for m in mon:
                            print m#, dict_numbs_to_patts[m], dict_numbs_to_perm_avoids_patt[m]
                    monitor.remove(mon)
                    #if ell < perm_len_max:
                    #    len_calc_patts.append(ell+1)

                    if limit_monitors:
                        if len(mon) == limit_monitors:
                            break
                        
                    saviors_filt = filter( lambda x : filter_saviors(x,mon) , saviors )
                    
                    if saviors_filt:
                        if detailed_report:
                            print "Extending this monitor with " + str(len(saviors_filt)) + " saviors"
                            print "They are " + str(saviors_filt)
                    
                        monitor.extend( map( lambda x : mon+[x], saviors_filt ) )
                    
                    if L in SG_keys and perm in SG[L].keys():
                        
                        larger_patts = sg[L][dict_clpatts_to_numbs[perm]]
                        
                        if detailed_report:
                            print "Extending this monitor with " + str(len(larger_patts)) + " larger patterns"
                            print "They are " + str(larger_patts)
                        
                        monitor.extend( map ( lambda x : mon+[(L,dict_clpatts_to_numbs[perm],x)], larger_patts ) )
            
            if capture_failure == True:
                if report:
                    print ""
                    print "There are " + str(len(monitor)) + " monitors"
                    print "Sorting the monitors"

                R = sorted(monitor, key = lambda x : len(x), reverse = True)
                newR = []

                if report:
                    print ""
                    print "Removing redundant monitors"
                for j,r in enumerate(R):
                    if not any(map(lambda s : gaur(s,r), R[j+1:])): #Reversing R[j+1:] should make this faster. Is the map lazy?
                        newR.append(r)

                if report:
                    print ""
                    print "There are now " + str(len(newR)) + " monitors"
                monitor = newR

    return monitor

def gaur(s,r):
    return all( map( lambda x : x in r, s ) )
                        
def filter_saviors(x,mon):

    # We want to return True if the underlying classical pattern of x is not in mon,
    # or if the shading of x has a higher number than any other shading already in mon
    # for this underlying classical pattern

    # Fix to make the running time O(length mon + length saviors)

    return True

    relevant_subset_of_mon = filter( lambda z : (z[0],z[1]) == (x[0],x[1]), mon)

    if not relevant_subset_of_mon:
        return True
    if max( map( lambda z : z[2], relevant_subset_of_mon ) ) < x[2]:
        return True

    return False

def one_from_each(values):
    
    LV = len(values)
    
    if LV == 0:
        return []
        
    if LV == 1:
        return map( lambda x: [x], values[0] )
    
    return sum( [map( lambda x: x+[v], one_from_each(values[1:])) for v in values[0]], [] )