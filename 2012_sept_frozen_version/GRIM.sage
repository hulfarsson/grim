from sage.combinat.permutation import to_standard # This is used for standardization of occurrences

# Attaching a file containing the subfuctions:
# rec - used in find_badpatts
# mesh_has_mesh - used for last step when we reduce output
load '~/repos/grim/2012_sept_frozen_version/GRIM_subfunctions.sage'

# In this version we mine the COMPLEMENT of the allowed patterns, since this saves us the time
# of calculating the complement later in para_find_badpatts

def find_badpatts(perm):
    
    global goodpatts

    n = len(perm) #WTF: We can skip calculating this by using j as input (maybe).
    
    if goodpatts[n].has_key(perm):

        #goodpatts[n][perm].reverse() # THE ORDER OF THIS MATTERS !!!!! WE NEED TO LOOK AT WHAT MAKES IT FASTEST!!!!

        #return rec( Set([]), Set([]), goodpatts[n][perm] )

        R = sorted(rec( Set([]), Set([]), goodpatts[n][perm] ), key = lambda x : len(x), reverse = True)
        newR = []

        for j,r in enumerate(R):
            if not any(map(lambda s : s.issubset(r), R[j+1:])):
                newR.append(r)

        return newR

    else:

        return [ Set([]) ]

#
# Below is the main algorithm
#

def GRIM(M,N,report=False):

    # global variables needed to be able to use parallelism
    global A
    global goodpatts

    badpatts = dict()
    outpatts = dict()
    
    interval = [1..M]

    if report:
        if 1 < M:
            print 'Starting search for allowed patterns of lengths 1...' + str(M)
        else:
             print 'Starting search for allowed patterns of length 1'
        print ''

    # INITIALIZING THE DICTIONARY goodpatts
    check_interval = []
    for j in interval:

        goodpatts[j] = dict()
        
        if len(A[j]) == factorial(j):
            for perm in A[j]:
                goodpatts[j][Permutation(perm)] = [Set([])]
        else:
            for perm in A[j]:
                goodpatts[j][Permutation(perm)] = [Set([])]
            check_interval.append(j)

    if not check_interval:
        print 'You need to search for longer patterns'
        return []

    minci = min(check_interval)
    maxci = max(check_interval)
    
    if report:
        if minci < maxci:
            print 'Only need to consider patterns of lengths ' + str(minci) + '...' + str(maxci)
        else:
            print 'Only need to consider patterns of length ' + str(minci)
        print ''

    if report:
        print 'Now looking at permutations of length'
        print ''

    for i in [1..N]:

        if report:
            print '         ' + str(i)

        min_maxci_i = min(maxci,i)

        for perm in A[i]:

            bla = better_max_patts_in_perm(perm,i,0,[],minci,min_maxci_i,False) #WTF: Remove bla?

    if report:
        print ''
        print 'Done'
        print ''

        for j in check_interval:
            print 'The number of allowed patterns of length ' + str(j) + ' is ' + str(sum(len(goodpatts[j][perm]) for perm in goodpatts[j].keys()))

        print ''
        print 'Getting rid of the unnecessary allowed patterns'
        print ''
    
    #WTF: Should this check be done when we are adding to goodpatts?
    for j in check_interval:
        for perm in goodpatts[j].keys():

            for R in goodpatts[j][perm]:
                listwoR = list(goodpatts[j][perm])
                listwoR.remove(R)

                if any(S.issubset(R) for S in listwoR):
                    goodpatts[j][perm] = listwoR

    if report:
        for j in check_interval:
            print 'The number of allowed patterns of length ' + str(j) + ' is now ' + str(sum(len(goodpatts[j][perm]) for perm in goodpatts[j].keys()))
        print ''

    # finding the forbidden patterns
        if minci < maxci:
            print 'Starting search for forbidden patterns of lengths ' + str(minci) + '...' + str(maxci)
        else:
            print 'Starting search for forbidden patterns of length ' + str(minci)
        print ''

    for j in check_interval: #WTF: Shouldn't this be check_interval?
        badpatts[j] = dict()
        for perm in Permutations(j):
            badpatts[j][perm] = find_badpatts(perm)
            #badpatts[j].extend( find_badpatts(perm) )

        if report:
            print 'The number of bad patterns of length ' + str(j) + ' is ' + str(sum(len(badpatts[j][perm]) for perm in badpatts[j].keys()))

    # finding the minimal forbidden patterns
    if report:
        print ''
        print 'Starting search for minimal forbidden patterns'
        print ''

    for j in check_interval: #WTF: Shouldn't this be check_interval?

        outpatts[j] = dict()

        rmax = (j+1)^2 + 1

        while sum(len(badpatts[j][perm]) for perm in badpatts[j].keys()) > 0:

            r = rmax

            for clpatt in badpatts[j].keys():
                for shading in badpatts[j][clpatt]:
                    rbp = shading.cardinality()

                    if rbp < r:
                        bump_clpatt = clpatt
                        bump_shading = shading
                        r = rbp

            if report:
                print 'Reducing with (' + str(bump_clpatt) + ', ' + str(bump_shading)

            if bump_clpatt in outpatts[j].keys():
                outpatts[j][bump_clpatt].append(bump_shading)
            else:
                outpatts[j][bump_clpatt] = [bump_shading]

            for i in filter(lambda x : x >= j, check_interval): #WTF: Shouldn't this be check_interval?

                for clpatt in badpatts[i].keys():

                    badpatts[i][clpatt] = filter( lambda sh: not mesh_has_mesh((clpatt,sh),(bump_clpatt,bump_shading)), badpatts[i][clpatt])
        
    return outpatts

#
# --------------------------------------------------------------------
#

#
# This function finds maximal mesh patterns inside permutations
#
# perm is the permutation we are mining from
# lenperm is the length of the perm
# loc is the location currently being examined
# chosen is the list of points chosen to be in a pattern
# Jmin is the smallest length of patterns we are looking for
# Jstop is the minimum of the length of the permutation and Jmax, the longest length of patterns

# Note that we do NOT want to get patterns of length j from a perm of length j
# because that is taken care of in another part of the algorithm

# Original call: better_max_patts_in_perm(perm,i,0,[],minci,min_maxci_i,False)

#WTF: This function should return a list of (cl_patt,R) which we then add (if necessary)
#     to goodpatts
def better_max_patts_in_perm(perm,lenperm,loc,chosen,Jmin,Jstop,calc):

    global goodpatts

    lc = len(chosen)

    # If there are too few elements in the perm left to complete a pattern of length Jmin we stop.
    if lenperm-loc+lc < Jmin:
        return []

    # If we have chosen too many elements.
    # WTF: Is this necessary?
    if lc > Jstop:
        return []

    if calc:
        
        if Jmin <= lc:

            vdik = dict()
            hdik = dict()

            v = 0
            h = 0

            for i in [0..lenperm-1]:

                permi = perm[i]

                if permi in chosen:
                    v = v+1
                else:
                    vdik[permi] = v

                if i+1 in chosen:
                    h = h+1
                else:
                    hdik[i+1] = h

            chosen_comp = filter(lambda x: x not in chosen, perm)

            R = Set(map(lambda x: (vdik[x],hdik[x]), chosen_comp))

            new_cl_patt = Permutation(to_standard(list(chosen)))

            if goodpatts[lc].has_key(new_cl_patt):

                if not any(U.issubset(R) for U in goodpatts[lc][new_cl_patt]):
                    goodpatts[lc][new_cl_patt].append(R)
            else:
                goodpatts[lc][new_cl_patt] = [R]

            if loc < lenperm and lc < (lenperm-1):
                return better_max_patts_in_perm(perm,lenperm,loc+1,chosen+[perm[loc]],Jmin,Jstop,True) + better_max_patts_in_perm(perm,lenperm,loc+1,chosen,Jmin,Jstop,False)
            else:
                return []

        else:

            if loc < lenperm and lc < (lenperm-1):
                return better_max_patts_in_perm(perm,lenperm,loc+1,chosen+[perm[loc]],Jmin,Jstop,True) + better_max_patts_in_perm(perm,lenperm,loc+1,chosen,Jmin,Jstop,False)
            else:
                return []
            
    else:

        if loc < lenperm and lc < (lenperm-1):
            return better_max_patts_in_perm(perm,lenperm,loc+1,chosen+[perm[loc]],Jmin,Jstop,True) + better_max_patts_in_perm(perm,lenperm,loc+1,chosen,Jmin,Jstop,False)
        else:
            return []

#
# --------------------------------------------------------------------
#