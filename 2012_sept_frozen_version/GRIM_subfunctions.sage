load '~/repos/basic_pattern_functions/basic_functions.sage'

# Helper function for find_badpatts and para_find_badpatts
def rec(C,forb,lst):
    
    #pruning lst
    newlst = []
    for L in lst:
        if L.issubset(forb):
            return []
        if not C.intersection(L):
            newlst.append(L)
    
    if newlst:
        lst0 = newlst[0]
        
        i = 0
        while lst0[i] in forb:
            i = i+1
            
        return rec( C.union(Set([lst0[i]])), forb, newlst ) + rec( C, forb.union(Set([lst0[i]])), newlst )
    else:
        return [C]

#
# This function checks if mesh_patt occurs in mesh_perm
#

def mesh_has_mesh(mesh_perm,mesh_patt):
    
    pat  = mesh_patt[0]
    R    = mesh_patt[1]
    
    perm = mesh_perm[0]
    S    = mesh_perm[1]
    
    k = len(pat)
    n = len(perm)

    if k > n:
        return False
    
    pat  = G(pat)
    perm = G(perm)
    
    Scomp = Set(map(lambda x: tuple(x),CartesianProduct([0..n],[0..n]))).difference(S)
    
    for H in Subwords(perm, k):
        
        X = dict(G(sorted(i for (i,_) in H)))
        Y = dict(G(sorted(j for (_,j) in H)))
        
        if H == [ (X[i], Y[j]) for (i,j) in pat ]:
            
            X[0], X[k+1] = 0, n+1
            Y[0], Y[k+1] = 0, n+1
            
            shady = ( X[i] < x < X[i+1] and Y[j] < y < Y[j+1]\
                      for (i,j) in R\
                      for (x,y) in perm\
                    )
                    
            shaky = ( X[i] <= x < X[i+1] and Y[j] <= y < Y[j+1]\
                      for (i,j) in R\
                      for (x,y) in Scomp\
                    )
                    
            if not any(shady):
                if not any(shaky):
                    return True
    return False

def describe_GRIM_output(OP):
    
    mult = 1
    flip = True
    
    for k in OP.keys():
        clpatts_of_this_length = OP[k].keys()
        print "There are " + str(len(clpatts_of_this_length)) + " underlying classical patterns of length " + str(k)
        for clpatt in clpatts_of_this_length:
            
            new_factor = len(OP[k][clpatt])
            mult = mult*new_factor
            
            print "There are " + str(new_factor) + " different shadings on " + str(clpatt)
        
        if flip:  
            print "The number of sets to monitor at the start of the clean-up phase is " + str(mult)
        flip = False

# Function that prints the sizes of all the sub-dictionaries of a big dictionary
def size_of_subdicts(D):
    for k in D.keys():
        print 'Perms of length ' + str(k) + ' with this property are ' + str(len(D[k]))

# Function checks whether the patterns in SG suffice to describe the permutations in the dictionary D up to length L
#
# THE STEP WHERE WE ARE COUNTING THE AVOIDERS SHOULD BE MADE FASTER BY CALLING THE FUNCTION ABOVE
#
def patterns_suffice(SG,L,D):

    # We start by flattening SG into the old format
    # This should NOT be done.
    # We should make a special avoiders function that
    # uses the structure of SG to run faster

    print "Flattening SG. You should fix this Henning!"
    #SG = sum( [ map( lambda x : (clpatt,x), SG[n][clpatt] ) for n in SG.keys() for clpatt in SG[n].keys()], [])
    
    for n in [1..L]:
    
        if n not in D.keys():
            print 'The dictionary does not contain permutations of length ' + str(n)
            break
        '''
        c = 0
        for w in Permutations(n):
    
            if avoids_mpats(w,SG) == True:
                c = c+1
        '''
        c = len(para_perms_in_avoiding_many_one_layer_many_shadings(n,SG))
        d = len(D[n])
    
        if c == d:
            print 'The patterns correctly describe the good perms of length ' + str(n)
        else:
            print ''
            print 'The patterns fail to describe the good perms of length ' + str(n)
            print 'There are ' + str(d) + ' good perms of length ' + str(n)
            print 'but there are ' + str(c) + ' perms that avoid the patterns'
            break