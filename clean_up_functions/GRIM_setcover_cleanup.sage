def clean_up(SG,perm_len_min,perm_len_max,patt_len_min,patt_len_max,report=False,detailed_report=False):

    out_sets = []
    # Dictionaries to go between mesh patterns
    # and numbers (length, pattern number, shading number)
    dict_clpatts_to_numbs = dict()
    dict_numbs_to_patts = dict()
    
    SG_keys = SG.keys()
    
    # A dictionary that "looks" like SG, but has only numbers
    sg = dict()
    
    # Putting values in the three dictionaries
    for k in SG_keys:
        
        sg[k] = dict()
        
        patt_i = 0
        
        for clpatt in SG[k]:
            
            sh_i = 0
            for sh in SG[k][clpatt]:
                dict_clpatts_to_numbs[clpatt] = patt_i
                dict_numbs_to_patts[(k,patt_i,sh_i)] = (clpatt,sh)
                sh_i = sh_i+1
                
            sg[k][patt_i] = [0..sh_i-1]
                
            patt_i = patt_i+1
            
    # initialize len_calc_patts as the length of the smallest mesh patterns
    len_calc_patts = [patt_len_min .. patt_len_max] #[SG_keys[0]]

    for L in [perm_len_min .. perm_len_max]:
        if report:
            print "Looking inside permutations of length " + str(L)

        for perm in B[L]:

            new_set = []

            for ell in filter( lambda x : x <= L, len_calc_patts):

                for patt_i in sg[ell].keys():
                    for sh_i in sg[ell][patt_i]:
                        
                        mpat = dict_numbs_to_patts[(ell,patt_i,sh_i)]
                        
                        if not avoids_mpat(perm,mpat[0],mpat[1]):
                            new_set.append((ell,patt_i,sh_i))

            out_sets.append(Set(new_set))

    if report:
        print ""
        print "Done scanning all the bad permutations"
        print "Creating the bases"

    R = sorted(rec( Set([]), Set([]), out_sets ), key = lambda x : len(x), reverse = True)

    lenR = len(R)

    if lenR == 0:
        if report:
            print "There are no bases, you probably need longer patterns"
        return R

    if lenR == 1:
        if report:
            print "There is just one base consisting of " + str(len(R[0])) + " pattern(s)"
        return R

    else:
        newR = []
        if report:
            print "There are " + str(lenR) + " bases"
        for j,r in enumerate(R):
            if not any(map(lambda s : s.issubset(r), R[j+1:])):
                newR.append(r)

        if report:
            print "There are now " + str(len(newR)) + " bases"

    return newR