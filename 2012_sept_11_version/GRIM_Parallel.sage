from sage.combinat.permutation import to_standard # This is used for standardization of occurrences

# Attaching a file containing the subfuctions:
# rec - used in find_badpatts
# mesh_has_mesh - used for last step when we reduce output
load '~/repos/grim/2012_sept_11_version/GRIM_subfunctions.sage'

#
# If run_parallel is set to True then the number of cpus will be automatically found
# Currently this only affects the step where we generate to bad patterns, but there is
# no visible speed-up since that step is now so fast. I need to parallelize the step where
# we search for the good patterns
#

# In this version we mine the COMPLEMENT of the allowed patterns, since this saves us the time
# of calculating the complement later in para_find_badpatts

#WTF: Different results each run because of parallelism!!!!!!!!!!!!!!!!!!!!!!!!
@parallel
def para_find_badpatts(perm,goodpatts_level):
    
    if goodpatts_level.has_key(perm):

        #goodpatts[n][perm].reverse() # THE ORDER OF THIS MATTERS !!!!! WE NEED TO LOOK AT WHAT MAKES IT FASTEST!!!!

        #print rec( Set([]), Set([]), goodpatts[n][perm] )
        #return rec( Set([]), Set([]), goodpatts[n][perm] )
        
        R = sorted(rec( Set([]), Set([]), goodpatts_level[perm] ), key = lambda x : len(x), reverse = True)
        newR = []

        for j,r in enumerate(R):
            if not any(map(lambda s : s.issubset(r), R[j+1:])):
                newR.append(r)

        return newR
        
    else:

        return [ Set([]) ]

#
# Below is the main algorithm
#

def GRIM_Parallel(M,N,report=False):

    global cps
    global check_interval
    global maxci
    global minci
    cps = sage.parallel.ncpus.ncpus()
    if report:
        print 'I detected ' + str(cps) + ' processors'
        print ''

    # global variables needed to be able to use parallelism
    global A

    goodpatts = dict()
    badpatts = dict()
    outpatts = dict()
    
    interval = [1..M]

    if report:
        if 1 < M:
            print 'Starting search for allowed patterns of lengths 1...' + str(M)
        else:
             print 'Starting search for allowed patterns of length 1'
        print ''

    # INITIALIZING THE DICTIONARY goodpatts
    check_interval = []
    for j in interval:

        goodpatts[j] = dict()
        
        if len(A[j]) == factorial(j):
            for perm in A[j]:
                goodpatts[j][Permutation(perm)] = [Set([])]
        else:
            for perm in A[j]:
                goodpatts[j][Permutation(perm)] = [Set([])]
            check_interval.append(j)

    if not check_interval:
        print 'You need to search for longer patterns'
        return []

    minci = min(check_interval)
    maxci = max(check_interval)
    
    if report:
        if minci < maxci:
            print 'Only need to consider patterns of lengths ' + str(minci) + '...' + str(maxci)
        else:
            print 'Only need to consider patterns of length ' + str(minci)
        print ''

    global temp_goodpatts
    temp_goodpatts = dict(goodpatts)

    many_dicts = map( lambda x: x[1], para_better_max_patts_in_perm(range(cps)) )

    for j in check_interval:
        goodpatts[j] = reduce(lambda d1,d2: merge(d1,d2), [D[j] for D in many_dicts])


    if report:
        print ''
        print 'Done'
        print ''

        for j in check_interval:
            print 'The number of allowed patterns of length ' + str(j) + ' is ' + str(sum(len(goodpatts[j][perm]) for perm in goodpatts[j].keys()))

        print ''
        print 'Getting rid of the unnecessary allowed patterns'
        print ''
    
    #WTF: Should this check be done when we are adding to goodpatts?
    for j in check_interval:
        for perm in goodpatts[j].keys():

            for R in goodpatts[j][perm]:
                listwoR = list(goodpatts[j][perm])
                listwoR.remove(R)

                if any(S.issubset(R) for S in listwoR):
                    goodpatts[j][perm] = listwoR

    if report:
        for j in check_interval:
            print 'The number of allowed patterns of length ' + str(j) + ' is now ' + str(sum(len(goodpatts[j][perm]) for perm in goodpatts[j].keys()))
        print ''

    # finding the forbidden patterns
        if minci < maxci:
            print 'Starting search for forbidden patterns of lengths ' + str(minci) + '...' + str(maxci)
        else:
            print 'Starting search for forbidden patterns of length ' + str(minci)
        print ''

    for j in check_interval:

        badpatts[j] = dict()
        prep_perms = map( lambda perm : (perm,goodpatts[j]), Permutations(j) )
        map( lambda x: assigning_to_dict(badpatts,j,x[0][0][0],x[1]), para_find_badpatts( prep_perms ) )
        #WTF: Why do we need x[0][0][0], and not just x[0][0]?

        if report: #WTF: MOVED AND CHANGED
            print 'The number of bad patterns of length ' + str(j) + ' is ' + str(sum(len(badpatts[j][perm]) for perm in badpatts[j].keys()))

    # finding the minimal forbidden patterns
    if report:
        print ''
        print 'Starting search for minimal forbidden patterns'
        print ''

    if len(check_interval) == 1:
        outpatts[check_interval[-1]] = dict()
        for cl_patt in badpatts[check_interval[-1]]:
            if badpatts[check_interval[-1]][cl_patt]:
                outpatts[check_interval[-1]][cl_patt] = badpatts[check_interval[-1]][cl_patt]
    else:
        
        # Reducing the bad patterns with small lengths
        for j in check_interval[:-1]:

            outpatts[j] = dict()

            rmax = (j+1)^2 + 1

            while sum(len(badpatts[j][perm]) for perm in badpatts[j].keys()) > 0:

                r = rmax

                for clpatt in badpatts[j].keys():
                    for shading in badpatts[j][clpatt]:
                        rbp = shading.cardinality()

                        if rbp < r:
                            bump_clpatt = clpatt
                            bump_shading = shading
                            r = rbp

                if report:
                    print 'Reducing with (' + str(bump_clpatt) + ', ' + str(bump_shading)

                if bump_clpatt in outpatts[j].keys():
                    outpatts[j][bump_clpatt].append(bump_shading)
                else:
                    outpatts[j][bump_clpatt] = [bump_shading]

                badpatts[j][bump_clpatt].remove(bump_shading)

                for i in filter(lambda x : x > j, check_interval):

                    for clpatt in badpatts[i].keys():

                        badpatts[i][clpatt] = filter( lambda sh: not mesh_has_mesh((clpatt,sh),(bump_clpatt,bump_shading)), badpatts[i][clpatt])

                    #badpatts[i] = filter(lambda bp: not mesh_has_mesh(bp,bump),badpatts[i])

        # Adding the longest patterns
        outpatts[check_interval[-1]] = dict()
        for cl_patt in badpatts[check_interval[-1]]:
            if badpatts[check_interval[-1]][cl_patt]:
                outpatts[check_interval[-1]][cl_patt] = badpatts[check_interval[-1]][cl_patt]
        
    return outpatts

#
# --------------------------------------------------------------------
#

#
# Parallel version of the above
#
# perm is the permutation we are mining from
# lenperm is the length of of perm
# loc is the location currently being examined
# chosen is the list of points chosen to be in a pattern
# Jmin is the smallest length of patterns we are looking for
# Jstop is the minimum of the length of the permutation and Jmax, the longest length of patterns

# Note that we do NOT want to get patterns of length j from a perm of length j
# because that is taken care of in another part of the algorithm

@parallel
def para_better_max_patts_in_perm(v):

    global A
    global cps
    global check_interval
    global maxci
    global temp_goodpatts

    my_goodpatts = dict(temp_goodpatts)

    for i in [1..N]:

        min_maxci_i = min(maxci,i)

        V = len(A[i])
    
        for j in xrange(v,V,cps):

            perm = A[i][j]
            bla = para_sub_better_max_patts_in_perm(perm,i,0,[],minci,min_maxci_i,False,my_goodpatts)

    for j in check_interval:
        for perm in my_goodpatts[j].keys():

            for R in my_goodpatts[j][perm]:
                listwoR = list(my_goodpatts[j][perm])
                listwoR.remove(R)

                if any(S.issubset(R) for S in listwoR):
                    my_goodpatts[j][perm] = listwoR
                    
    return my_goodpatts

#
# --------------------------------------------------------------------
#

#
# This function finds maximal mesh patterns inside permutations
#
# perm is the permutation we are mining from
# lenperm is the length of of perm
# loc is the location currently being examined
# chosen is the list of points chosen to be in a pattern
# Jmin is the smallest length of patterns we are looking for
# Jstop is the minimum of the length of the permutation and Jmax, the longest length of patterns

# Note that we do NOT want to get patterns of length j from a perm of length j
# because that is taken care of in another part of the algorithm

#WTF: This function should return a list of (cl_patt,R) which we then add (if necessary)
#     to goodpatts  
def para_sub_better_max_patts_in_perm(perm,lenperm,loc,chosen,Jmin,Jstop,calc,my_goodpatts):

    lc = len(chosen)

    if lenperm-loc+lc < Jmin:
        return []

    if lc > Jstop:
        return []

    if calc:
        
        if Jmin <= lc:

            vdik = dict()
            hdik = dict()

            v = 0
            h = 0

            for i in [0..lenperm-1]:

                permi = perm[i]

                if permi in chosen:
                    v = v+1
                else:
                    vdik[permi] = v

                if i+1 in chosen:
                    h = h+1
                else:
                    hdik[i+1] = h

            chosen_comp = filter(lambda x: x not in chosen, perm)

            R = Set(map(lambda x: (vdik[x],hdik[x]), chosen_comp))

            new_cl_patt = Permutation(to_standard(list(chosen)))

            if my_goodpatts[lc].has_key(new_cl_patt):

                if not any(U.issubset(R) for U in my_goodpatts[lc][new_cl_patt]):
                    my_goodpatts[lc][new_cl_patt].append(R)
            else:
                my_goodpatts[lc][new_cl_patt] = [R]

            if loc < lenperm and lc < (lenperm-1):
                return para_sub_better_max_patts_in_perm(perm,lenperm,loc+1,chosen+[perm[loc]],Jmin,Jstop,True,my_goodpatts) + para_sub_better_max_patts_in_perm(perm,lenperm,loc+1,chosen,Jmin,Jstop,False,my_goodpatts)
            else:
                return []

        else:

            if loc < lenperm and lc < (lenperm-1):
                return para_sub_better_max_patts_in_perm(perm,lenperm,loc+1,chosen+[perm[loc]],Jmin,Jstop,True,my_goodpatts) + para_sub_better_max_patts_in_perm(perm,lenperm,loc+1,chosen,Jmin,Jstop,False,my_goodpatts)
            else:
                return []
            
    else:

        if loc < lenperm and lc < (lenperm-1):
            return para_sub_better_max_patts_in_perm(perm,lenperm,loc+1,chosen+[perm[loc]],Jmin,Jstop,True,my_goodpatts) + para_sub_better_max_patts_in_perm(perm,lenperm,loc+1,chosen,Jmin,Jstop,False,my_goodpatts)
        else:
            return []
#
# --------------------------------------------------------------------
#

# Function for merging two dictionaries
def merge(d1, d2):
    """
    Merges two dictionaries, non-destructively, combining 
    values on duplicate keys as defined by the optional merge
    function.

    Examples:

    >>> d1
    {'a': [1], 'c': [3], 'b': [2]}
    >>> merge(d1, d1)
    {'a': [1,1], 'c': [3,3], 'b': [2,2]}

    """
    result = dict(d1)
    for k,v in d2.iteritems():
        if k in result:
            result[k].extend(v)
        else:
            result[k] = v
    return result

#
# --------------------------------------------------------------------
#

# Function for adding in a dictionary, inside a call to map

def assigning_to_dict(dik,j,key,entry):
    dik[j][key] = entry
 
