'''
The function post_proc_shading_lemma is for cleaning up the output from
GRIM using the shading lemma. This needs some work.
'''

def post_proc_shading_lemma(L):
    
    newL = map(lambda x: (x[0],shading_lemma(x)), L)
    
    for i in [0..len(L)-1]:
        
        H = newL[i]
        
        #if any( mesh_has_mesh_family(H,K) for K in newL if K != H):
            
        for j in [0..(i-1),(i+1)..len(L)-1]:
            
            K = newL[j]
            
            if mesh_has_mesh_family(H,K):
                print str(i) + ' can be removed based on ' + str(j)
            
def mesh_has_mesh_family(H,K):
    
    for a in H[1]:
        
        if any( mesh_has_mesh( (H[0],a),(K[0],b) ) for b in K[1]):
            return True
            
    return False

def shading_lemma(mpat):
    
    pot = []
    
    perm = mpat[0]
    n = len(perm)
    
    for i in [0..len(perm)-1]:
        pot.extend( map(lambda x: ((i+1,perm[i]),x), [(1,1),(0,1),(0,0),(1,0)]) )
    
    known = [mpat[1]]
    alive = list(known)
    
    while alive:
        
        [known,alive] = shading_lemma_U(known,alive,pot,n)
        
    return known
    
def shading_lemma_U(known,alive,pot,n):
    
    newknown = list(known)
    
    for K in alive:
        
        Kchange = 0
        
        for p in filter(lambda p: movable(p,K,n), pot):
            
            box = (p[0][0]-1+p[1][0],p[0][1]-1+p[1][1])
            
            if box in K:
                L = Set(K).difference(Set([box]))
                if L not in newknown:
                    newknown.append(L)
                    alive.append(L)
                    
                    Kchange = 1
                    
            else:
                L = Set(K).union(Set([box]))
                if L not in newknown:
                    newknown.append(L)
                    alive.append(L)
                    
                    Kchange = 1
                    
        if Kchange == 0:
            alive.remove(K)
                    
    return [newknown,alive]
                
def movable(p,K,n):
    
    # If the box diagonally from p is in K then p is not movable
    if (p[0][0]-p[1][0],p[0][1]-p[1][1]) in K:
        return False
        
    # If the two neighboring boxes are in K then p is not movable
    if (p[0][0]-p[1][0],p[0][1]-1+p[1][1]) in K and ((p[0][0]-1+p[1][0],p[0][1]-p[1][1])) in K:
        return False
    
    # Check conditions on boxes other than the three closest ones
    if any((p[0][0]-p[1][0],i) in K and (p[0][0]-1+p[1][0],i) not in K for i in [0..(p[0][1]-2),(p[0][1]+1)..n]):
        return False
    
    if any((i,p[0][1]-p[1][1]) in K and (i,p[0][1]-1+p[1][1]) not in K for i in [0..(p[0][0]-2),(p[0][0]+1)..n]):
        return False
        
    return True