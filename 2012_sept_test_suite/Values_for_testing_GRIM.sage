import time

'''
The following list contains (group,example,largest_good_perms)
'''

Fixed_largest_bad_patterns_for_GRIM = dict()
Fixed_largest_bad_patterns_for_GRIM[(0,1,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(0,2,7)] = [3]

Fixed_largest_bad_patterns_for_GRIM[(1,1,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(1,2,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(1,3,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(1,4,7)] = [5]

Fixed_largest_bad_patterns_for_GRIM[(2,1,8)] = [3]
Fixed_largest_bad_patterns_for_GRIM[(2,2,7)] = [6]
Fixed_largest_bad_patterns_for_GRIM[(2,3,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(2,4,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(2,6,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(2,7,8)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(2,8,8)] = [3]

Fixed_largest_bad_patterns_for_GRIM[(3,1,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(3,2,10)] = [3]
Fixed_largest_bad_patterns_for_GRIM[(3,3,8)] = [3]

Fixed_largest_bad_patterns_for_GRIM[(4,1,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(4,2,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(4,3,8)] = [6]
Fixed_largest_bad_patterns_for_GRIM[(4,4,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(4,5,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(4,6,8)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(4,7,8)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(4,8,7)] = [6]
Fixed_largest_bad_patterns_for_GRIM[(4,9,9)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(4,10,8)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(4,11,7)] = [5]

Fixed_largest_bad_patterns_for_GRIM[(5,1,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(5,2,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(5,3,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(5,4,7)] = [5]

Fixed_largest_bad_patterns_for_GRIM[(6,1,6)] = [4]

Fixed_largest_bad_patterns_for_GRIM[(7,1,7)] = [5]
Fixed_largest_bad_patterns_for_GRIM[(7,2,7)] = [5]

Fixed_largest_bad_patterns_for_GRIM[(8,1,9)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(8,2,7)] = [4]
Fixed_largest_bad_patterns_for_GRIM[(8,3,7)] = [4]

Fixed_largest_bad_patterns_for_GRIM[(9,1,8)] = [5]

Fixed_values_for_good_perms = sorted(Fixed_largest_bad_patterns_for_GRIM.keys())

def run_tests(module_name,repeats=1):

    load module_name

    global A

    failed_tests = []
    test_times    = []

    for (gr,ex,size) in Fixed_values_for_good_perms:
        # Get a precomputed dictionary
        A = load('/Users/ulfarsson/repos/grim/2012_sept_test_suite/good_perm_dicts/' + str((gr,ex,size)) + '.sobj')

        for M in Fixed_largest_bad_patterns_for_GRIM[gr,ex,size]:
            times_for_M = []
            for j in [1..repeats]:
                # Get a precomputed output from GRIM
                know = load('/Users/ulfarsson/repos/grim/2012_sept_test_suite/correct_GRIM_outputs/' + str((gr,ex,size)) + '_' + str(M) + '.sobj')

                goodpatts = dict()

                start_time = time.time()
                test = GRIM(M,size)
                end_time = time.time()

                run_time = end_time - start_time
                times_for_M.append(run_time)

                if not compare_two_GRIM_outputs(know,test):
                    print "FAILURE: The test " + str((gr,ex,size)) + " with pattern lengths " + str(M) + " on try " + str(j)
                    failed_tests.append(((gr,ex,size),M,j))
                else:
                    print "The test " + str((gr,ex,size)) + " with pattern lengths " + str(M) + " passed on try " + str(j)
                    print "It took " + str(run_time) + " seconds"
                print ""

            test_times.append( (gr,ex,size,M, min(times_for_M) ) )

    if not failed_tests:
        print "All tests passed!"
    else:
        print "There were " + str(len(failed_tests)) + " tests"
    return failed_tests, test_times



def compare_two_GRIM_outputs(test,know):
    
    test_keys = test.keys()
    know_keys = know.keys()
    
    if len(test.keys()) != len(know.keys()):
        print "The lengths of the patterns do not match!"
        return False
        
    for k in test_keys:
        if k in know_keys:
            test_cl_patts = test[k]
            know_cl_patts = know[k]
            
            if len(test_cl_patts) != len(know_cl_patts):
                print "The classical patterns of length " + str(k) + " do not match!"
                return False
                
            for cl_patt in test_cl_patts:
                if cl_patt in know_cl_patts:
                    test_shadings = test[k][cl_patt]
                    know_shadings = know[k][cl_patt]
                    
                    if len(test_shadings) != len(know_shadings):
                        print "The shadings of the classical pattern " + str(cl_patt) + " do not match!"
                        return False
                        
                    for sh in test_shadings:
                        if not sh in know_shadings:
                            "The shading " + str(sh) + " for the classical pattern " + str(cl_patt) + " is in the test dictionary but not in the known dictionary"
                else:
                    print "The classical pattern " + str(cl_patt) + " in the test dictionary is not in the known dictionary"
                    return False
                    
        else:
            print "The pattern length " + str(k) + " in the test dictionary is not in the known dictionary"
            return False
            
    return True