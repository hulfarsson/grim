'''
Load the values that will be tested
'''
load '~/repos/grim/2012_sept_test_suite/values_for_testing_GRIM.sage'

'''
The test file should contain a function called GRIM.
You can also choose how many times you want to repeat each test.
'''
module_name = '~/repos/grim/GRIM.sage'
repeats = 2


'''
If you want to save the times the tests tooks, as well as any failures
'''
save_time       = True
save_version    = "2012 September 12 -- with Hjalti's mining code"
save_test_suite = "2012_sept"
save_where      = '/Users/ulfarsson/repos/grim/timings/Timings_of_tests.txt'
save_machine    = "Henning's laptop"

A = dict()
goodpatts = dict()

failures,times = run_tests(module_name,repeats)

'''
The following code saves the output from the tests to a file located in the subfolder
timings of the grim repo
'''

curr = time.localtime()
tota = sum(map(lambda x: x[4], times))

if save_time:
	f = open(save_where,'a')
	f.write("Machine: " + save_machine)
	f.write('\n')
	f.write("GRIM version: " + save_version)
	f.write('\n')
	f.write("Test_suite: " + save_test_suite)
	f.write('\n')
	f.write("Total time was " + str(floor(tota/60)) + " minutes and " + str(mod(ceil(tota),60)) + " seconds")
	f.write('\n')
	f.write("Test performed on " + str(curr[0]) + "-" + str(curr[1]) + "-" + str(curr[2]) + " at " + str(curr[3]) + ":" + str(curr[4]))
	f.write('\n')
	f.write(str(times))
	f.write('\n')
	f.write('\n')
	f.close()